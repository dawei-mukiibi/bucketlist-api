"""This module initializes the app"""
import os
from .app import create_app
from .models import db, User, BucketList, BucketItem

app = create_app(os.getenv('APP_CONFIG'))

from .views import *
