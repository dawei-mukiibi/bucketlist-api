#!/usr/bin/env bash
set -e

echo "Connecting to postgres host and waiting for postgres to start..."

while ! /bin/nc -z localhost 5432; do
  sleep 0.1
done

echo "PostgreSQL started"

python dbmigration.py db init
python dbmigration.py db migrate
python dbmigration.py db upgrade

nosetests --with-coverage --cover-package=bucketlist

